package com.kbmsi.kbmsiapps;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.heinrichreimersoftware.materialintro.app.IntroActivity;
import com.heinrichreimersoftware.materialintro.slide.SimpleSlide;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Rinov on 9/22/2017.
 */

public class BaseActivity extends IntroActivity {
    static String [][] linkFoto;
    int count;
    final static String vUpdate = "1.0.6";
    void intro(){

    }
    void setDataLink(){
        linkFoto = new String[8][5];
        count = 0;
        linkFoto[count][1] = "Website KBMSI";
        linkFoto[count][2] = "http://3.bp.blogspot.com/-nPurn-lS8I4/WcUYmzas5eI/AAAAAAAACGk/d4oPwA9uT4cByKFy_uVWXF1n_WLhxaI-gCK4BGAYYCw/s1600/kbmsi.png";
        linkFoto[count][3] = "http://kbmsi.filkom.ub.ac.id/";
        linkFoto[count][4] = "";
        ++count;
        linkFoto[count][1] = "WANGSIT";
        linkFoto[count][2] = "http://2.bp.blogspot.com/-z-Z01hXe5oQ/WcUayDA-yvI/AAAAAAAACHM/7H7J7ALpAy80wKnStIAkic9PRPjB1aoOgCK4BGAYYCw/s1600/wangsit.png";
        linkFoto[count][3] = "http://kbmsi.filkom.ub.ac.id/wangsit";
        linkFoto[count][4] = "Warung Angkringan Sistem Informasi Hits";
        ++count;
        linkFoto[count][1] = "S-KRIM";
        linkFoto[count][2] = "http://1.bp.blogspot.com/-UtpYE3F5WfA/WcUerPrFGaI/AAAAAAAACHY/lBgzd7I47DwtP3wiW6n2tljqfLZzfDyUwCK4BGAYYCw/s1600/skrim.png";
        linkFoto[count][3] = "http://skrim.kbmsi.or.id/";
        linkFoto[count][4] = "Sistem Informasi - Kelompok Riset Mahasiswa";
        ++count;
        linkFoto[count][1] = "ISHOT 2.0";
        linkFoto[count][2] = "http://2.bp.blogspot.com/-co27U3BqmLo/WcUerPdqduI/AAAAAAAACHg/DBzgvubx2F81zIQ__RFE-s6MbAVNtV6fwCK4BGAYYCw/s1600/ishot.png";
        linkFoto[count][3] = "http://ishot.kbmsi.or.id/";
        linkFoto[count][4] = "Information System Hot Trends 2.0";
        ++count;
        linkFoto[count][1] = "MRFEST 2.0";
        linkFoto[count][2] = "http://3.bp.blogspot.com/-T5HRydEdtQo/WcUffYwqBEI/AAAAAAAACH8/lY5xlGFOliAU8eLKJkJ7Rm6UAqUP-5gkQCK4BGAYYCw/s1600/mrfest.png";
        linkFoto[count][3] = "http://mrfest.kbmsi.or.id/";
        linkFoto[count][4] = "Malang Revolutive Festival 2.0";
        ++count;
        linkFoto[count][1] = "WOWSI 17";
        linkFoto[count][2] = "http://1.bp.blogspot.com/-5TamdH0xyXo/WcUer2l-z8I/AAAAAAAACHo/9kWDQtQOl3AgFxtZIV2ULt1Y9A1HcVg9ACK4BGAYYCw/s1600/wow.png";
        linkFoto[count][3] = "http://wowsi.kbmsi.or.id/";
        linkFoto[count][4] = "Weeks of Welcoming KBMSI 2017";
        ++count;
        linkFoto[count][1] = "SILUET";
        linkFoto[count][2] = "http://1.bp.blogspot.com/-79yrYM_fvX4/WcVEXsynREI/AAAAAAAACIM/U2cC09sJ6-gLSgJgx1-fZMtvbsklIkF_gCK4BGAYYCw/s1600/siluet.png";
        linkFoto[count][3] = "http://kbmsi.filkom.ub.ac.id/magazine/";
        linkFoto[count][4] = "Sistem Informasi Lugas Edukatif dan Terdepan";
        ++count;
        linkFoto[count][1] = "ABOUT";
        linkFoto[count][2] = "https://2.bp.blogspot.com/-5VS4pMM8nBg/WcSuJ4hHCCI/AAAAAAAACEI/ACJnYKTV-VMuzeuk98xOZsfVfnIR_UJcwCLcBGAs/s1600/Logo-05.png";
        linkFoto[count][3] = "";
        linkFoto[count][4] = "Sistem Informasi Lugas Edukatif dan Terdepan";
    }
    private ProgressDialog mProgressDialog;

    public void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setMessage("Loading...");
        }

        mProgressDialog.show();
    }

    public void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }



}
