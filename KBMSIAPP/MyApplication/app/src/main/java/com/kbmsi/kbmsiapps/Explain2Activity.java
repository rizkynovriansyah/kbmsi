package com.kbmsi.kbmsiapps;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.customtabs.CustomTabsClient;
import android.support.customtabs.CustomTabsIntent;
import android.support.customtabs.CustomTabsServiceConnection;
import android.support.customtabs.CustomTabsSession;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.intrusoft.library.FrissonView;
import com.ndroid.nadim.sahel.CoolToast;

public class Explain2Activity extends BaseActivity {

    FrissonView frissonView;
    ImageView logo, backButton;
    Context context;
    CardView image;
    TextView customtabs, name, about;
    Activity a;

    public static final String CUSTOM_TAB_PACKAGE_NAME = "com.android.chrome";

    CustomTabsClient mClient;
    CustomTabsSession mCustomTabsSession;
    CustomTabsServiceConnection mCustomTabsServiceConnection;
    CustomTabsIntent customTabsIntent;

    static String URL = "http://www.google.com/";
    CoolToast coolToast;
    DatabaseReference myRef, namaproker1, linkgambar, linkvisit, nama, penjelasan, update, visitstream, maintenance;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_explain2);
        showProgressDialog();
        Intent get = getIntent();
        int data = Integer.parseInt(get.getStringExtra("data"));
        myRef = null;
        namaproker1 = null;
        linkgambar = null;
        linkvisit = null;
        nama= null;
        penjelasan = null;
        update = null;
        visitstream = null; maintenance = null;
        firebaseSet(data);
        context = getApplicationContext();
        a = this;
        coolToast = new CoolToast(this);
        frissonView = (FrissonView) findViewById(R.id.wave_head);
        image = (CardView) findViewById(R.id.image);
        logo = (ImageView) findViewById(R.id.imageView);
        backButton = (ImageView) findViewById(R.id.backButton);
        customtabs = (TextView) findViewById(R.id.customtabs);
        name = (TextView) findViewById(R.id.name);
        about = (TextView) findViewById(R.id.about);
//        visitStream = (TextView) findViewById(R.id.visitStream);
        updateSche = (TextView) findViewById(R.id.updateSche);
        penjelasannya = (TextView) findViewById(R.id.penjelasannya);

        readFire();

        setDataLink();
        URL = linkFoto[data][3];
//        URL = "http://kbmsi.filkom.ub.ac.id/";

        //costum
        /*
            Setup Chrome Custom Tabs
         */
        mCustomTabsServiceConnection = new CustomTabsServiceConnection() {
            @Override
            public void onCustomTabsServiceConnected(ComponentName componentName, CustomTabsClient customTabsClient) {
                mClient= customTabsClient;
                mClient.warmup(0L);
                mCustomTabsSession = mClient.newSession(null);
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                mClient= null;
            }
        };

        CustomTabsClient.bindCustomTabsService(getApplication(), CUSTOM_TAB_PACKAGE_NAME, mCustomTabsServiceConnection);

        customTabsIntent = new CustomTabsIntent.Builder(mCustomTabsSession)
                .setShowTitle(true)
                .build();
        /*
            End custom tabs setup
         */

        //costum

        name.setText(linkFoto[data][1]);
        about.setText(linkFoto[data][4]);
        String linkHeader = "" + linkFoto[data][2];
        String linkLogo = "" + linkFoto[data][2];
        System.out.println("parsing datanya " + linkHeader);
        useGlide("header", linkHeader);
        useGlide("logo", linkLogo);

        switch (data) {
            case 0: {
                frissonView.setTintColor(getResources().getColor(R.color.kbmsi));
                image.setCardBackgroundColor(getResources().getColor(R.color.putih));
                System.out.println("urlnya : "+URL);

                break;
            }
            case 1: {
                frissonView.setTintColor(getResources().getColor(R.color.wangsit));
                image.setCardBackgroundColor(getResources().getColor(R.color.putih));

//                frissonView.setRadialGradient(R.color.kbmsi, R.color.wangsit);
                break;
            }
            case 2: {
                frissonView.setTintColor(getResources().getColor(R.color.skrim));
                image.setCardBackgroundColor(getResources().getColor(R.color.putih));

//                frissonView.setRadialGradient(R.color.kbmsi, R.color.skrim);
                break;
            }
            case 3: {
                frissonView.setTintColor(getResources().getColor(R.color.ishot));
                image.setCardBackgroundColor(getResources().getColor(R.color.putih));

//                frissonView.setRadialGradient(R.color.kbmsi, R.color.ishot);
                break;
            }
            case 4: {
                frissonView.setTintColor(getResources().getColor(R.color.mrfest));
                image.setCardBackgroundColor(getResources().getColor(R.color.putih));

//                frissonView.setRadialGradient(R.color.kbmsi, R.color.mrfest);
                break;
            }
            case 5: {
                frissonView.setTintColor(getResources().getColor(R.color.wowsi));
                image.setCardBackgroundColor(getResources().getColor(R.color.putih));

//                frissonView.setRadialGradient(R.color.kbmsi, R.color.wowsi);
                break;
            }
            case 6: {
                frissonView.setTintColor(getResources().getColor(R.color.siluet));
                image.setCardBackgroundColor(getResources().getColor(R.color.putih));

//                frissonView.setRadialGradient(R.color.kbmsi, R.color.wowsi);
                break;
            }
        }

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        customtabs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (maint){
                    coolToast.make("under maintenance", CoolToast.INFO);
                } else {
                    customTabsIntent.launchUrl(Explain2Activity.this, Uri.parse(URL));
                }
            }
        });
    }

    TextView visitStream, updateSche, penjelasannya;
    FirebaseDatabase database;

    boolean maint;

    void firebaseSet(int data){
        database = FirebaseDatabase.getInstance();

        myRef = database.getReference("proker");
        switch (data){
            case 0 :{
                namaproker1 = myRef.child("website");
                break;
            }
            case 1 :{
                namaproker1 = myRef.child("wangsit");
                break;
            }
            case 2 :{
                namaproker1 = myRef.child("skrim");
                break;
            }
            case 3 :{
                namaproker1 = myRef.child("ishot");
                break;
            }
            case 4 :{
                namaproker1 = myRef.child("mrfest");
                break;
            }
            case 5 :{
                namaproker1 = myRef.child("wowsi");
                break;
            }
            case 6 :{
                namaproker1 = myRef.child("siluet");
                break;
            }
        }

        linkgambar= namaproker1.child("linkgambar");
        linkvisit= namaproker1.child("linkvisit");
        nama = namaproker1.child("nama");
        penjelasan = namaproker1.child("penjelasan");
        update = namaproker1.child("update");
        visitstream = namaproker1.child("visit");
        maintenance = namaproker1.child("maintenance");
    }

    void readFire(){
        visitstream.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                String value = dataSnapshot.getValue(String.class);
//                visitStream.setText(value);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value

            }
        });
        update.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                String value = dataSnapshot.getValue(String.class);
                updateSche.setText(value);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value

            }
        });
        penjelasan.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                String value = dataSnapshot.getValue(String.class);
                penjelasannya.setText(value);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value

            }
        });
        maintenance.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                String value = dataSnapshot.getValue(String.class);
                int nilai = Integer.parseInt(value);
                if (nilai == 1){
                    maint = true;
//                    Toast.makeText(getApplicationContext(), "True", Toast.LENGTH_SHORT).show();
                } else {
                    maint = false;
//                    Toast.makeText(getApplicationContext(), "False", Toast.LENGTH_SHORT).show();
                }
                hideProgressDialog();
            }
            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value

            }
        });
    }


    void useGlide(final String linkapa, String link) {
//        Glide.with(getApplicationContext())
//                .load(link)
//                .asBitmap()
//                .centerCrop()
//                .into(new SimpleTarget<Bitmap>(SimpleTarget.SIZE_ORIGINAL, SimpleTarget.SIZE_ORIGINAL) {
//                    @Override
//                    public void onResourceReady(Bitmap bitmap, GlideAnimation anim) {
//                        // Do something with bitmap here.
//                        switch (linkapa){
//                            case "header" : {
//                                frissonView.setBitmap(bitmap);
//                                break;
//                            }
//                            case "logo" : {
//                                logo.setImageBitmap(bitmap);
//                                break;
//                            }
//                        }
//                    }
//                });
//        showProgressDialog();
        Glide.with(this)
                .load(link)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .into(logo);

        Glide.with(this)
                .load(link)
                .asBitmap()
                .into(new SimpleTarget<Bitmap>(SimpleTarget.SIZE_ORIGINAL, SimpleTarget.SIZE_ORIGINAL) {
                    @Override
                    public void onResourceReady(Bitmap bitmap, GlideAnimation anim) {
                        // Do something with bitmap here.
                        frissonView.setBitmap(bitmap);
                    }
                });
    }

}
