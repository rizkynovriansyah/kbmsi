package com.kbmsi.kbmsiapps;

import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.intrusoft.library.FrissonView;
import com.ndroid.nadim.sahel.CoolToast;

import static android.provider.ContactsContract.CommonDataKinds.Website.URL;

public class ExplainActivity extends BaseActivity {
    FrissonView frissonView;
    ImageView backButtons;
    TextView develop, tap, versioning;
    CoolToast coolToast;
    boolean temp, tempColor;
    DatabaseReference myRef, aboutnya, lovecounter;
    int lovenya;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_explain);
        showProgressDialog();
        temp = false;
        FirebaseDatabase database;
        tap = (TextView) findViewById(R.id.tap);
        versioning = (TextView) findViewById(R.id.versioning);
        versioning.setText("v"+vUpdate);

        database = FirebaseDatabase.getInstance();
        myRef = database.getReference("proker");
        aboutnya = myRef.child("about");
        lovecounter = aboutnya.child("lovecounter");

        lovecounter.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                String value = dataSnapshot.getValue(String.class);

                lovenya = Integer.parseInt(value);
                if (temp){
                    develop.setText("❤ "+lovenya+" hearts");
//                    develop.setTextColor(getResources().getColor(R.color.p2s));
                    if (tempColor){
                        animation1();
                    } else {
                        animation2();
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value

            }
        });

        coolToast = new CoolToast(this);
        develop = (TextView) findViewById(R.id.develop);
        develop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (temp){
                    develop.setTextSize(20);
                    develop.setText("❤ "+lovenya+" hearts");

                } else {
                    coolToast.setIcon(R.drawable.ic_insert_emoticon_black_24dp);
                    coolToast.make("Best Regards, P2S!", CoolToast.SUCCESS, CoolToast.SHORT);
                    temp = true;
                }
                lovecounter.setValue("" + (++lovenya));
                tap.setVisibility(View.GONE);
            }
        });
        backButtons = (ImageView) findViewById(R.id.backButtons);
        backButtons.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        String load = "https://2.bp.blogspot.com/-5VS4pMM8nBg/WcSuJ4hHCCI/AAAAAAAACEI/ACJnYKTV-VMuzeuk98xOZsfVfnIR_UJcwCLcBGAs/s1600/Logo-05.png";
        frissonView = (FrissonView) findViewById(R.id.wave_head);
        Glide.with(getApplication())
                .load(R.drawable.logops)
                .asBitmap()
                .into(new SimpleTarget<Bitmap>(SimpleTarget.SIZE_ORIGINAL, SimpleTarget.SIZE_ORIGINAL) {
                    @Override
                    public void onResourceReady(Bitmap bitmap, GlideAnimation anim) {
                        frissonView.setBitmap(bitmap);
                        hideProgressDialog();
                    }
                });
    }
    void animation1() {
        int colorFrom = getResources().getColor(R.color.putih);
        int colorTo = getResources().getColor(R.color.p2s);
        ValueAnimator colorAnimation = ValueAnimator.ofObject(new ArgbEvaluator(), colorFrom, colorTo);
        colorAnimation.setDuration(100); // milliseconds
        colorAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

            @Override
            public void onAnimationUpdate(ValueAnimator animator) {
                develop.setTextColor((int) animator.getAnimatedValue());
            }

        });
        colorAnimation.start();
    }

    void animation2() {
        int colorTo = getResources().getColor(R.color.p2s);
        int colorFrom = getResources().getColor(R.color.putih);
        ValueAnimator colorAnimation = ValueAnimator.ofObject(new ArgbEvaluator(), colorFrom, colorTo);
        colorAnimation.setDuration(100); // milliseconds
        colorAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

            @Override
            public void onAnimationUpdate(ValueAnimator animator) {
                develop.setTextColor((int) animator.getAnimatedValue());
            }

        });
        colorAnimation.start();
    }
}
