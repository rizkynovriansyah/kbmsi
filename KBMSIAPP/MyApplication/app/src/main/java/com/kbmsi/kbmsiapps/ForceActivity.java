package com.kbmsi.kbmsiapps;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ndroid.nadim.sahel.CoolToast;

public class ForceActivity extends AppCompatActivity {

    TextView versionnew;
    LinearLayout feedback;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_force);
        Intent get = getIntent();
        feedback = (LinearLayout) findViewById(R.id.buttonUpdate);
        versionnew = (TextView) findViewById(R.id.versionnew);
        String versi = get.getStringExtra("versi");

        feedback.setBackgroundColor(getResources().getColor(R.color.kbmsi));

        versionnew.setText("Version "+versi+" Available");
        feedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                } catch (Exception e){

                }
            }
        });
    }
}
