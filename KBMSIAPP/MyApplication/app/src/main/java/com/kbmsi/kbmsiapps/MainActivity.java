package com.kbmsi.kbmsiapps;

import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.graphics.drawable.TransitionDrawable;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.heinrichreimersoftware.materialintro.slide.SimpleSlide;
import com.ndroid.nadim.sahel.CoolToast;


import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends BaseActivity {
    CircleImageView logokbmsi, logowangsit, logoskrim, logoishot, logomrfest, logowowsi, logosiluet, logoaboutus;
    LinearLayout background, kbmsi, wangsit, skrim, ishot, mrfest, wowsi, about, siluet;

    private FirebaseAuth mAuth;
    int visitWebsite, visitWangsit, visitSkrim, visitIshot, visitMrfest, visitWowsi, visitSiluet, visitAbout;

    Button feedback;
    boolean needToUpdate;
    String updateVer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        showProgressDialog();

        //firebase
        mAuth = FirebaseAuth.getInstance();
        firebaseAnon();
        firebaseSet();

        visitstream1.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                String value = dataSnapshot.getValue(String.class);
                visitWebsite = Integer.parseInt(value);

                a = true;
//                hideindialog();
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value

            }
        });
        visitstream2.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                String value = dataSnapshot.getValue(String.class);
                visitWangsit = Integer.parseInt(value);

                b = true;
//                hideindialog();
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value

            }
        });
        visitstream3.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                String value = dataSnapshot.getValue(String.class);
                visitSkrim = Integer.parseInt(value);

                c = true;
//                hideindialog();
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value

            }
        });
        visitstream4.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                String value = dataSnapshot.getValue(String.class);
                visitIshot = Integer.parseInt(value);

                d = true;
//                hideindialog();
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value

            }
        });
        visitstream5.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                String value = dataSnapshot.getValue(String.class);
                visitMrfest = Integer.parseInt(value);

                e = true;
//                hideindialog();
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value

            }
        });
        visitstream6.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                String value = dataSnapshot.getValue(String.class);
                visitWowsi = Integer.parseInt(value);

                f = true;

//                hideindialog();
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value

            }
        });
        visitstream7.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                String value = dataSnapshot.getValue(String.class);
                visitSiluet = Integer.parseInt(value);

                g = true;
//                hideindialog();
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value

            }
        });

        visitstreamabout.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                String value = dataSnapshot.getValue(String.class);
                visitAbout = Integer.parseInt(value);

                hideindialog();
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value

            }
        });
        update.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                String value = dataSnapshot.getValue(String.class);
                final String appPackageName = getPackageName();
                if (!value.equalsIgnoreCase(vUpdate)){
                    feedback.setText("New Update to "+value+" !");
                    feedback.setBackgroundColor(getResources().getColor(R.color.kbmsi));
                    feedback.setTextColor(getResources().getColor(R.color.putih));
                } else {
                    feedback.setBackgroundColor(getResources().getColor(R.color.grey));
                }
                updateVer = value;
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value

            }
        });

        forceupdate.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                String value = dataSnapshot.getValue(String.class);
                if (value.equalsIgnoreCase("1") && !updateVer.equalsIgnoreCase(vUpdate) ){
                    if (!value.equalsIgnoreCase(vUpdate)){
                        Intent nextAct = new Intent(getApplicationContext(), ForceActivity.class);
                        nextAct.putExtra("versi", updateVer);
                        startActivity(nextAct);
                        finish();
                    } else {

                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value

            }
        });

        //firebase
        background = (LinearLayout) findViewById(R.id.lineBase);
        feedback = (Button) findViewById(R.id.feedback);
        kbmsi = (LinearLayout) findViewById(R.id.kbmsi);
        wangsit = (LinearLayout) findViewById(R.id.wangsit);
        skrim = (LinearLayout) findViewById(R.id.skrim);
        ishot = (LinearLayout) findViewById(R.id.ishot);
        mrfest = (LinearLayout) findViewById(R.id.mrfest);
        wowsi = (LinearLayout) findViewById(R.id.wowsi);
        about = (LinearLayout) findViewById(R.id.about);
        siluet = (LinearLayout) findViewById(R.id.siluet);
        logokbmsi = (CircleImageView) findViewById(R.id.logokbmsi);
        logowangsit = (CircleImageView) findViewById(R.id.logowangsit);
        logoishot = (CircleImageView) findViewById(R.id.logoishot);
        logoskrim = (CircleImageView) findViewById(R.id.logoskrim);
        logomrfest = (CircleImageView) findViewById(R.id.logomrfest);
        logowowsi = (CircleImageView) findViewById(R.id.logowowsi);
        logosiluet = (CircleImageView) findViewById(R.id.logosiluet);
        logoaboutus = (CircleImageView) findViewById(R.id.logoaboutus);
        clickListerner();
        setData();

        final CoolToast toaster = new CoolToast(this);

        feedback.setBackgroundColor(getResources().getColor(R.color.kbmsi));
        feedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                } catch (Exception e){
                    toaster.make("Something Missing", CoolToast.WARNING);
                }
            }
        });

    }
    boolean a,b,c,d,e, f, g;
    boolean selamatDatang;
    void hideindialog(){
        final CoolToast toaster = new CoolToast(this);
        if (a && b && c && d && e && !selamatDatang){
            toaster.make("Welcome!", CoolToast.INFO);
            selamatDatang = true;
        }
        hideProgressDialog();
    }

    void IntentAct(int counter) {
        if (counter != 7) {
            Intent nextAct = new Intent(getApplicationContext(), Explain2Activity.class);
            nextAct.putExtra("data", counter + "");
            startActivity(nextAct);
        } else {
            Intent nextAct = new Intent(getApplicationContext(), ExplainActivity.class);
            nextAct.putExtra("data", counter + "");
            startActivity(nextAct);
        }
    }

    FirebaseDatabase database;
    DatabaseReference forceupdate, update, myRef, namaproker1, linkgambar1, nama1, visitstream1;
    DatabaseReference namaproker2, linkgambar2, nama2, visitstream2;
    DatabaseReference namaproker3, linkgambar3, nama3, visitstream3;
    DatabaseReference namaproker4, linkgambar4, nama4, visitstream4;
    DatabaseReference namaproker5, linkgambar5, nama5, visitstream5;
    DatabaseReference namaproker6, linkgambar6, nama6, visitstream6;
    DatabaseReference namaproker7, linkgambar7, nama7, visitstream7;
    DatabaseReference aboutnya, visitstreamabout, lovecounter;

    void firebaseSet() {
        database = FirebaseDatabase.getInstance();
        myRef = database.getReference("proker");
        update = database.getReference("update");
        forceupdate = database.getReference("forceupdate");
        namaproker1 = myRef.child("website");
        linkgambar1 = namaproker1.child("linkgambar");
        nama1 = namaproker1.child("nama");
        visitstream1 = namaproker1.child("visit");

        namaproker2 = myRef.child("wangsit");
        linkgambar2 = namaproker2.child("linkgambar");
        nama2 = namaproker2.child("nama");
        visitstream2 = namaproker2.child("visit");

        namaproker3 = myRef.child("skrim");
        linkgambar3 = namaproker3.child("linkgambar");
        nama3 = namaproker3.child("nama");
        visitstream3 = namaproker3.child("visit");

        namaproker4 = myRef.child("ishot");
        linkgambar4 = namaproker4.child("linkgambar");
        nama4 = namaproker4.child("nama");
        visitstream4 = namaproker4.child("visit");

        namaproker5 = myRef.child("mrfest");
        linkgambar5 = namaproker5.child("linkgambar");
        nama5 = namaproker5.child("nama");
        visitstream5 = namaproker5.child("visit");

        namaproker6 = myRef.child("wowsi");
        linkgambar6 = namaproker6.child("linkgambar");
        nama6 = namaproker6.child("nama");
        visitstream6 = namaproker6.child("visit");

        namaproker7 = myRef.child("siluet");
        linkgambar7 = namaproker7.child("linkgambar");
        nama7 = namaproker7.child("nama");
        visitstream7 = namaproker7.child("visit");

        aboutnya = myRef.child("about");
        visitstreamabout = aboutnya.child("visit");
    }

    void clickListerner() {
        kbmsi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                IntentAct(0);
                // Read from the database
                visitstream1.setValue("" + (++visitWebsite));
            }
        });
        wangsit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                IntentAct(1);
                visitstream2.setValue("" + (++visitWangsit));

            }
        });
        skrim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                IntentAct(2);
                visitstream3.setValue("" + (++visitSkrim));
            }
        });
        ishot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                IntentAct(3);
                visitstream4.setValue("" + (++visitIshot));
            }
        });
        mrfest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                IntentAct(4);
                visitstream5.setValue("" + (++visitMrfest));
            }
        });
        wowsi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                IntentAct(5);
                visitstream6.setValue("" + (++visitWowsi));
            }
        });
        siluet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                IntentAct(6);
                visitstream7.setValue("" + (++visitSiluet));
            }
        });
        about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                IntentAct(7);
                visitstreamabout.setValue("" + (++visitAbout));
            }
        });
    }

    void animation() {
        animation1();
    }

    int counter;

    void setData() {
        setDataLink();
        counter = 0;
        useGlide(counter, linkFoto[counter][2]);
        useGlide(++counter, linkFoto[counter][2]);
        useGlide(++counter, linkFoto[counter][2]);
        useGlide(++counter, linkFoto[counter][2]);
        useGlide(++counter, linkFoto[counter][2]);
        useGlide(++counter, linkFoto[counter][2]);
        useGlide(++counter, linkFoto[counter][2]);
        useGlide(++counter, linkFoto[counter][2]);
//        linkFoto[counter][1] = "aboutus";
//        linkFoto[counter][2] = "";

    }

    void firebaseAnon() {
        final String TAG = "WADU";
        mAuth.signInAnonymously()
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInAnonymously:success");
                            FirebaseUser user = mAuth.getCurrentUser();

                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInAnonymously:failure", task.getException());
                            Toast.makeText(MainActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }

                        // ...
                    }
                });

    }

    void animation1() {
        int colorFrom = getResources().getColor(R.color.kbmsi);
        int colorTo = getResources().getColor(R.color.wangsit);
        ValueAnimator colorAnimation = ValueAnimator.ofObject(new ArgbEvaluator(), colorFrom, colorTo);
        colorAnimation.setDuration(10000); // milliseconds
        colorAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

            @Override
            public void onAnimationUpdate(ValueAnimator animator) {
                background.setBackgroundColor((int) animator.getAnimatedValue());

            }

        });
        colorAnimation.start();
    }

    void animation2() {
        int colorTo = getResources().getColor(R.color.kbmsi);
        int colorFrom = getResources().getColor(R.color.wangsit);
        ValueAnimator colorAnimation = ValueAnimator.ofObject(new ArgbEvaluator(), colorFrom, colorTo);
        colorAnimation.setDuration(10000); // milliseconds
        colorAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

            @Override
            public void onAnimationUpdate(ValueAnimator animator) {
                background.setBackgroundColor((int) animator.getAnimatedValue());
            }

        });
        colorAnimation.start();
    }

    void useGlide(int linkapa, String link) {
//        Glide.with(this)
//                .load(link)
//                .crossFade()
//                .diskCacheStrategy(DiskCacheStrategy.ALL)
//                .into(logo);

        switch (linkapa) {
            case 0: {
                Glide.with(this)
                        .load(link)
                        .crossFade()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(logokbmsi);

                break;
            }
            case 1: {
                Glide.with(this)
                        .load(link)
                        .crossFade()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(logowangsit);
                break;
            }
            case 3: {
                Glide.with(this)
                        .load(link)
                        .crossFade()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(logoishot);

                break;
            }
            case 2: {
                Glide.with(this)
                        .load(link)
                        .crossFade()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(logoskrim);

                break;
            }
            case 4: {
                Glide.with(this)
                        .load(link)
                        .crossFade()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(logomrfest);

                break;
            }
            case 5: {
                Glide.with(this)
                        .load(link)
                        .crossFade()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(logowowsi);

                break;
            }
            case 6: {
                Glide.with(this)
                        .load(link)
                        .crossFade()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(logosiluet);

                break;
            }
            case 7: {
                Glide.with(this)
                        .load(link)
                        .crossFade()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(logoaboutus);
                break;
            }
            default: {

            }
        }

        Glide.with(getApplicationContext())
                .load(link)
                .asBitmap()
                .thumbnail(0.5f)
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(new SimpleTarget<Bitmap>(SimpleTarget.SIZE_ORIGINAL, SimpleTarget.SIZE_ORIGINAL) {
                    @Override
                    public void onResourceReady(Bitmap bitmap, GlideAnimation anim) {

                    }
                });
//        hideProgressDialog();
    }

    FirebaseUser currentUser;

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        currentUser = mAuth.getCurrentUser();
    }
}
