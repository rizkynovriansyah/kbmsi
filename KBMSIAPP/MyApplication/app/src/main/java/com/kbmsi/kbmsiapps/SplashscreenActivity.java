package com.kbmsi.kbmsiapps;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.heinrichreimersoftware.materialintro.app.IntroActivity;
import com.heinrichreimersoftware.materialintro.slide.SimpleSlide;

public class SplashscreenActivity extends IntroActivity {

    private static int SPLASH_DISPLAY_LENGTH = 4000;

    int visitWebsite, visitWangsit, visitSkrim, visitIshot, visitMrfest, visitWowsi, visitSiluet, visitAbout;
    FirebaseDatabase database;
    DatabaseReference update, myRef, namaproker1, linkgambar1, nama1, visitstream1;
    DatabaseReference namaproker2, linkgambar2, nama2, visitstream2;
    DatabaseReference namaproker3, linkgambar3, nama3, visitstream3;
    DatabaseReference namaproker4, linkgambar4, nama4, visitstream4;
    DatabaseReference namaproker5, linkgambar5, nama5, visitstream5;
    DatabaseReference namaproker6, linkgambar6, nama6, visitstream6;
    DatabaseReference namaproker7, linkgambar7, nama7, visitstream7;
    DatabaseReference aboutnya, visitstreamabout, lovecounter;
    private FirebaseAuth mAuth;

    void firebaseSet() {
        mAuth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance();
        myRef = database.getReference("proker");
        update = database.getReference("update");
        namaproker1 = myRef.child("website");
        linkgambar1 = namaproker1.child("linkgambar");
        nama1 = namaproker1.child("nama");
        visitstream1 = namaproker1.child("visit");

        namaproker2 = myRef.child("wangsit");
        linkgambar2 = namaproker2.child("linkgambar");
        nama2 = namaproker2.child("nama");
        visitstream2 = namaproker2.child("visit");

        namaproker3 = myRef.child("skrim");
        linkgambar3 = namaproker3.child("linkgambar");
        nama3 = namaproker3.child("nama");
        visitstream3 = namaproker3.child("visit");

        namaproker4 = myRef.child("ishot");
        linkgambar4 = namaproker4.child("linkgambar");
        nama4 = namaproker4.child("nama");
        visitstream4 = namaproker4.child("visit");

        namaproker5 = myRef.child("mrfest");
        linkgambar5 = namaproker5.child("linkgambar");
        nama5 = namaproker5.child("nama");
        visitstream5 = namaproker5.child("visit");

        namaproker6 = myRef.child("wowsi");
        linkgambar6 = namaproker6.child("linkgambar");
        nama6 = namaproker6.child("nama");
        visitstream6 = namaproker6.child("visit");

        namaproker7 = myRef.child("siluet");
        linkgambar7 = namaproker7.child("linkgambar");
        nama7 = namaproker7.child("nama");
        visitstream7 = namaproker7.child("visit");

        aboutnya = myRef.child("about");
        visitstreamabout = aboutnya.child("visit");

        visitstream1.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                String value = dataSnapshot.getValue(String.class);
                visitWebsite = Integer.parseInt(value);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value

            }
        });
        visitstream2.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                String value = dataSnapshot.getValue(String.class);
                visitWangsit = Integer.parseInt(value);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value

            }
        });
        visitstream3.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                String value = dataSnapshot.getValue(String.class);
                visitSkrim = Integer.parseInt(value);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value

            }
        });
        visitstream4.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                String value = dataSnapshot.getValue(String.class);
                visitIshot = Integer.parseInt(value);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value

            }
        });
        visitstream5.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                String value = dataSnapshot.getValue(String.class);
                visitMrfest = Integer.parseInt(value);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value

            }
        });
        visitstream6.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                String value = dataSnapshot.getValue(String.class);
                visitWowsi = Integer.parseInt(value);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value

            }
        });
        visitstream7.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                String value = dataSnapshot.getValue(String.class);
                visitSiluet = Integer.parseInt(value);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value

            }
        });

        visitstreamabout.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                String value = dataSnapshot.getValue(String.class);
                visitAbout = Integer.parseInt(value);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value

            }
        });
        update.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                String value = dataSnapshot.getValue(String.class);
                final String appPackageName = getPackageName();

//                if (!value.equalsIgnoreCase(vUpdate)){
//                    needToUpdate = true;
//                    feedback.setText("Update to "+value);
//                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value

            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);
        onNewIntent(getIntent());
         /* New Handler to start the Menu-Activity
         * and close this Splash-Screen after some seconds.*/
        firebaseSet();
        try{
            new Handler().postDelayed(new Runnable(){
                @Override
                public void run() {
                /* Create an Intent that will start the Menu-Activity. */
                    Intent mainIntent = new Intent(SplashscreenActivity.this, MainActivity.class);
                    SplashscreenActivity.this.startActivity(mainIntent);
                    SplashscreenActivity.this.finish();
                }
            }, SPLASH_DISPLAY_LENGTH);

        } catch (Exception e){
            Intent next = new Intent(getApplicationContext(),SplashscreenActivity.class);
            startActivity(next);
        }
    }
}
