package id.ac.ub.filkom.kbmsi.wapss.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import id.ac.ub.filkom.kbmsi.wapss.Adapters.RecentTopicsAdapter;
import id.ac.ub.filkom.kbmsi.wapss.Adapters.SubForumsAdapter;
import id.ac.ub.filkom.kbmsi.wapss.Analytics.MyApplication;
import id.ac.ub.filkom.kbmsi.wapss.Models.PostImages;
import id.ac.ub.filkom.kbmsi.wapss.Models.RecentTopics;
import id.ac.ub.filkom.kbmsi.wapss.Models.SubForum;
import id.ac.ub.filkom.kbmsi.wapss.R;
import id.ac.ub.filkom.kbmsi.wapss.Utilities.Config;

public class ForumActivity extends AppCompatActivity {

    ProgressDialog dialog;

    ArrayList<SubForum> subForums = new ArrayList<>();
    private ArrayList<RecentTopics> RecentTopics = new ArrayList<>();

    private RecyclerView subForumRecyclerView;
    private SubForumsAdapter subForumAdapter;
    private RecyclerView.LayoutManager subForumLayoutManager;

    private RecyclerView topicRecyclerView;
    private RecentTopicsAdapter topicAdapter;
    private RecyclerView.LayoutManager topicLayoutManager;

    private String forum_id;
    private String sub_forum_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forum);

        Intent intent = getIntent();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        sub_forum_name = intent.getStringExtra("sub_forum_name");
        if (sub_forum_name == null){
            getSupportActionBar().setTitle(R.string.sub_forum);
        }else{
            getSupportActionBar().setTitle(sub_forum_name);
        }

        subForumRecyclerView = (RecyclerView) findViewById(R.id.sub_forum);
        topicRecyclerView = (RecyclerView) findViewById(R.id.recycleviewTopicForum);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        subForumRecyclerView.setHasFixedSize(true);
        topicRecyclerView.setHasFixedSize(true);


        // use a linear layout manager
        subForumLayoutManager = new LinearLayoutManager(this);
        subForumRecyclerView.setLayoutManager(subForumLayoutManager);
        topicLayoutManager = new LinearLayoutManager(this);
        topicRecyclerView.setLayoutManager(topicLayoutManager);

        forum_id = intent.getStringExtra("forum_id");
        if (forum_id == null){
            getForum();
            //getTopicsOnForum("-1");
        }else{
            getTopicsOnForum(forum_id);
            getSubForum(forum_id);
        }


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }

    public void getForum() {
        dialog = ProgressDialog.show(this, "", "Loading...");
        RequestQueue queue = Volley.newRequestQueue(this);

        // Instantiate the RequestQueue.
        JsonArrayRequest jsArrRequest = new JsonArrayRequest
                (Request.Method.GET, Config.API_ALL_SUB_FORUM, null, new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {
                        //Toast.makeText(ForumActivity.this, response.toString(), Toast.LENGTH_LONG).show();
                        jsonParseSubForum(response);
                    }

                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(ForumActivity.this, "Forum tidak tersedia", Toast.LENGTH_LONG).show();
                    }
                });

        queue.add(jsArrRequest);
    }

    public void getSubForum(String forum_id) {
        dialog = ProgressDialog.show(this, "", "Loading...");
        RequestQueue queue = Volley.newRequestQueue(this);

        // Instantiate the RequestQueue.
        JsonArrayRequest jsArrRequest = new JsonArrayRequest
                (Request.Method.GET, Config.API_GET_SUB_FORUM+forum_id, null, new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {
                        //Toast.makeText(ForumActivity.this, response.toString(), Toast.LENGTH_LONG).show();
                        jsonParseSubForum(response);
                    }

                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(ForumActivity.this, "Sub Forum tidak tersedia", Toast.LENGTH_LONG).show();
                    }
                });

        queue.add(jsArrRequest);
    }

    public void jsonParseSubForum(JSONArray response) {
        if (response != null) {
            /*try{
                System.out.println(response.getJSONObject(response.length()-1).toString());
            }catch (JSONException e){
                e.printStackTrace();
            }*/

            try {
                for (int i = 0; i < response.length(); i++){
                    //Log.d("hmm", "" + i);
                    JSONObject object = response.getJSONObject(i);
                    //Log.d("hmm", object.toString());
                    ArrayList<SubForum> subSubForum = new ArrayList<>();

                    JSONArray json_sub_forum = object.getJSONArray("sub_forum");
                        /*Log.d("sub_forum", object.getJSONArray("sub_forum").toString());*/
                    //JSONArray json_sub_forum = object.getJSONArray("sub_forum");

                    for (int a = 0; a < json_sub_forum.length(); a++){
                        JSONObject obj = json_sub_forum.getJSONObject(a);

                        if (obj.getInt("id") != -2){ //wanjjjeeerrrr ini perlu dibenahi!!!
                        subSubForum.add(
                                new SubForum(
                                        obj.getString("id"),
                                        obj.getString("name"),
                                        obj.getString("description"),
                                        null
                                ));
                        }



                        //Log.d("sub_forum", "" + a);
                    }
                    subForums.add(new SubForum(
                            object.getString("id"),
                            object.getString("name"),
                            object.getString("description"),
                            subSubForum
                    ));

                    subForumAdapter = new SubForumsAdapter(ForumActivity.this, subForums);
                    subForumRecyclerView.setAdapter(subForumAdapter);
                }
            }catch (JSONException e) {
                e.printStackTrace();
            }



        }


        dialog.dismiss();
    }

    public void getTopicsOnForum(String forum_id){
        //dialog = ProgressDialog.show(this,"","Loading...");
        RequestQueue queue = Volley.newRequestQueue(this);

        // Instantiate the RequestQueue.
        JsonArrayRequest jsArrRequest = new JsonArrayRequest
                (Request.Method.GET, Config.API_GET_TOPIC_ON_SUB_FORUM+forum_id, null, new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {
                        //Toast.makeText(ForumActivity.this, response.toString(), Toast.LENGTH_LONG).show();
                        jsonParseRecentTopics(response);
                    }

                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(ForumActivity.this, "Tidak ada Topic di sini", Toast.LENGTH_LONG).show();
                    }
                });

        queue.add(jsArrRequest);
    }

    public void jsonParseRecentTopics(JSONArray json){

        if (!json.equals("null")){
            try {
                for (int i = 0; i < json.length(); i++ ){

                    ArrayList<PostImages> listPostImages = new ArrayList<>();
                    JSONObject object = json.getJSONObject(i);
                    if (object != null){
                        JSONArray postImages = object.getJSONArray("images");

                        if (postImages != null){
                            for (int j = 0; j  < postImages.length(); j++){
                                JSONObject obj = postImages.getJSONObject(j);

                                listPostImages.add(new PostImages(obj.getString("url")));

                            }
                        }
                    }

                    RecentTopics.add(
                            new RecentTopics(
                                    object.getInt("post_id"),
                                    object.getString("topic_id"),
                                    object.getString("auth_pict"),
                                    object.getString("auth_name"),
                                    object.getString("auth_title"),
                                    object.getString("sub_forum"),
                                    object.getString("post_title"),
                                    listPostImages,
                                    object.getString("views"),
                                    object.getString("replies"),
                                    object.getString("post_time")
                            )
                    );

                }

                topicAdapter = new RecentTopicsAdapter(ForumActivity.this, RecentTopics);
                topicRecyclerView.setAdapter(topicAdapter);
            }catch (JSONException e){
                e.printStackTrace();
            }
        }
        /*dialog.dismiss();*/
    }


}
