package id.ac.ub.filkom.kbmsi.wapss.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import id.ac.ub.filkom.kbmsi.wapss.R;
import id.ac.ub.filkom.kbmsi.wapss.Utilities.CircleTransform;

public class ImageActivity extends AppCompatActivity {

    private ImageView image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image);

        image = (ImageView) findViewById(R.id.imagePostView);

        Intent intent = getIntent();

        Picasso.with(this).load(intent.getStringExtra("images")).into(image);

    }
}
