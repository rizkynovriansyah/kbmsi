package id.ac.ub.filkom.kbmsi.wapss.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import id.ac.ub.filkom.kbmsi.wapss.Adapters.TopicAdapter;
import id.ac.ub.filkom.kbmsi.wapss.MainActivity;
import id.ac.ub.filkom.kbmsi.wapss.Models.Post;
import id.ac.ub.filkom.kbmsi.wapss.R;
import id.ac.ub.filkom.kbmsi.wapss.Utilities.Config;


public class TopicActivity extends AppCompatActivity {

    private ArrayList<Post> Topic = new ArrayList<>();
    private ProgressDialog dialog;

    private RecyclerView topicRecyclerView;
    private TopicAdapter topicAdapter;
    private RecyclerView.LayoutManager topicLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_topic);

        Intent intent = getIntent();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        getSupportActionBar().setTitle(intent.getStringExtra("sub_forum"));

        topicRecyclerView = (RecyclerView) findViewById(R.id.recycleviewTopic);
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        topicRecyclerView.setHasFixedSize(true);


        // use a linear layout manager
        topicLayoutManager = new LinearLayoutManager(this);
        topicRecyclerView.setLayoutManager(topicLayoutManager);

        getTopic(intent.getStringExtra("topic_id"));
        //updateViewers(intent.getStringExtra("topic_id"));


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }



    public void updateViewers(final String topic_id) {
        RequestQueue queue = Volley.newRequestQueue(this);

        // Instantiate the RequestQueue.
        StringRequest request = new StringRequest(Request.Method.POST, Config.API_UPDATE_VIEWERS,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);
                    }
                },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(TopicActivity.this, "Error : " + error, Toast.LENGTH_LONG).show();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("topic_id", topic_id);

                return params;
            }
        };
        queue.add(request);
    }

    public void getTopic(String topic_id) {
        dialog = ProgressDialog.show(this, "", "Loading...");
        RequestQueue queue = Volley.newRequestQueue(this);

        // Instantiate the RequestQueue.
        JsonArrayRequest jsArrRequest = new JsonArrayRequest
                (Request.Method.GET, (Config.API_GET_TOPIC+topic_id), null, new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {
                        //Toast.makeText(TopicActivity.this, response.toString(), Toast.LENGTH_LONG).show();
                        jsonParseTopic(response);
                    }

                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(TopicActivity.this, "Error : " + error, Toast.LENGTH_LONG).show();
                    }
                });

        queue.add(jsArrRequest);
    }

    public void jsonParseTopic(JSONArray response) {
        if (response != null) {
            /*try{
                System.out.println(response.getJSONObject(response.length()-1).toString());
            }catch (JSONException e){
                e.printStackTrace();
            }*/

            try {
                for (int i = 0; i < response.length(); i++){
                    //Log.d("hmm", "" + i);
                    JSONObject object = response.getJSONObject(i);
                    Log.d("topic", object.toString());
                    Topic.add(new Post(
                            object.getString("post_id"),
                            object.getString("sub_forum"),
                            object.getString("post_title"),
                            object.getString("auth_pict"),
                            object.getString("auth_name"),
                            object.getString("auth_title"),
                            object.getString("starter_name"),
                            object.getString("post_time"),
                            object.getString("post_content"),
                            object.getString("views"),
                            object.getString("replies")
                    ));

                    topicAdapter = new TopicAdapter(TopicActivity.this, Topic);
                    topicRecyclerView.setAdapter(topicAdapter);
                }
            }catch (JSONException e) {
                e.printStackTrace();
            }



        }


        dialog.dismiss();
    }
}
