package id.ac.ub.filkom.kbmsi.wapss.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import id.ac.ub.filkom.kbmsi.wapss.Activities.ImageActivity;
import id.ac.ub.filkom.kbmsi.wapss.Activities.TopicActivity;
import id.ac.ub.filkom.kbmsi.wapss.Models.PostImages;
import id.ac.ub.filkom.kbmsi.wapss.R;

/**
 * Created by E5471 on 01/11/2016.
 */

public class PostImagesAdapter extends RecyclerView.Adapter<PostImagesAdapter.ViewHolder> {

    private ArrayList<PostImages> listPostImages = new ArrayList<>();
    private Context context;

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public ImageView post_image;
        public String imagesUrl;

        public ViewHolder(View view) {
            super(view);
            post_image = (ImageView) view.findViewById(R.id.post_image);

            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Intent intent = new Intent(context, ImageActivity.class);
            intent.putExtra("images", imagesUrl);

            context.startActivity(intent);
            //Toast.makeText(context, imagesUrl,Toast.LENGTH_SHORT).show();
        }
    }

    public PostImagesAdapter(Context context, ArrayList<PostImages> listPostImages) {
        this.listPostImages = listPostImages;
        this.context = context;
    }

    @Override
    public PostImagesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,int viewType) {
        // create a new view
        View vi = LayoutInflater.from(parent.getContext()).inflate(R.layout.post_images_adapter, parent, false);
        // set the view's size, margins, paddings and layout parameters

        PostImagesAdapter.ViewHolder vh = new PostImagesAdapter.ViewHolder(vi);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(PostImagesAdapter.ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        PostImages image = listPostImages.get(position);
        //Picasso.with(holder.post_image.getContext()).load(image.getUrl()).into(holder.post_image);
        Picasso
                .with(context)
                .load(image.getUrl())
                .resize(300, 300)
                .centerCrop()
                .into(holder.post_image);

        holder.imagesUrl = image.getUrl();

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return (null != listPostImages? listPostImages.size() : 0);
    }
}
