package id.ac.ub.filkom.kbmsi.wapss.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import id.ac.ub.filkom.kbmsi.wapss.Activities.TopicActivity;
import id.ac.ub.filkom.kbmsi.wapss.Models.RecentTopics;
import id.ac.ub.filkom.kbmsi.wapss.R;
import id.ac.ub.filkom.kbmsi.wapss.Utilities.CircleTransform;

/**
 * Created by E5471 on 31/10/2016.
 */

public class RecentTopicsAdapter extends RecyclerView.Adapter<RecentTopicsAdapter.ViewHolder> {

    private ArrayList<RecentTopics> ListRecentTopics;
    private static Context context;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        // each data item is just a string in this case
        public ImageView auth_pict;
        public TextView auth_name, auth_title, sub_forum, post_title, post_views, post_replies, post_time;
        public String topic_id;

        public RecyclerView imagesRecyclerView;
        public PostImagesAdapter imagesAdapter;
        public RecyclerView.LayoutManager imagesLayoutManager;

        public ViewHolder(View view) {
            super(view);
            auth_pict = (ImageView) view.findViewById(R.id.auth_pict);
            auth_name = (TextView) view.findViewById(R.id.auth_name);
            auth_title = (TextView) view.findViewById(R.id.auth_title);
            sub_forum = (TextView) view.findViewById(R.id.sub_forum);
            post_title = (TextView) view.findViewById(R.id.post_title);
            post_views = (TextView) view.findViewById(R.id.post_views);
            post_replies = (TextView) view.findViewById(R.id.post_replies);
            post_time = (TextView) view.findViewById(R.id.post_time);


            imagesRecyclerView = (RecyclerView) view.findViewById(R.id.post_image_recycle_view);
            imagesRecyclerView.setHasFixedSize(true);

            imagesLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
            imagesRecyclerView.setLayoutManager(imagesLayoutManager);

            view.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            //Toast.makeText(context, auth_name.getText().toString(),Toast.LENGTH_SHORT).show();

            Intent intent = new Intent(context, TopicActivity.class);

            //intent.putExtra("auth_name", auth_name.getText().toString());
            //intent.putExtra("topic_title", post_title.getText().toString());
            intent.putExtra("sub_forum", sub_forum.getText().toString());
            intent.putExtra("topic_id",  topic_id);

            context.startActivity(intent);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public RecentTopicsAdapter(Context context, ArrayList<RecentTopics> listRecentTopics) {
        this.ListRecentTopics = listRecentTopics;
        this.context = context;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public RecentTopicsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,int viewType) {
        // create a new view
        View vi = LayoutInflater.from(parent.getContext()).inflate(R.layout.recent_topic_adapter, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(vi);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        RecentTopics topic = ListRecentTopics.get(position);

        holder.topic_id = topic.getTopic_id();

        Picasso.with(holder.auth_pict.getContext())
                .load(topic.getAuth_pict())
                .transform(new CircleTransform())
                .into(holder.auth_pict);

        holder.auth_name.setText(topic.getAuth_name());
        holder.auth_title.setText(topic.getAuth_title());
        holder.sub_forum.setText(topic.getSub_forum());
        holder.post_title.setText(topic.getPost_title());
        holder.post_views.setText(topic.getViews());
        holder.post_replies.setText(topic.getReplies());
        holder.post_time.setText(topic.getPost_time());

        holder.imagesAdapter = new PostImagesAdapter(context, topic.getPost_images());
        holder.imagesRecyclerView.setAdapter(holder.imagesAdapter);

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return (null != ListRecentTopics? ListRecentTopics.size() : 0);
    }


}
