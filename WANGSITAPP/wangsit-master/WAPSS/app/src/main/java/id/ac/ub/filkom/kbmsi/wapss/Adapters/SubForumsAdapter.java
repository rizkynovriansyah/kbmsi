package id.ac.ub.filkom.kbmsi.wapss.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import id.ac.ub.filkom.kbmsi.wapss.Activities.ForumActivity;
import id.ac.ub.filkom.kbmsi.wapss.Models.SubForum;
import id.ac.ub.filkom.kbmsi.wapss.R;

/**
 * Created by E5471 on 02/11/2016.
 */

public class SubForumsAdapter extends RecyclerView.Adapter<SubForumsAdapter.ViewHolder> {

    private ArrayList<SubForum> listSubForum = new ArrayList<>();
    private Context context;

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        public TextView sub_forum_name;
        public TextView sub_forum_description;
        public View divider;
        public String forum_id;
        public RecyclerView subForumSubForumRecyclerView;
        public SubForumsSubForumsAdapter subForumSubForumAdapter;
        public RecyclerView.LayoutManager subForumSubForumLayoutManager;

        public ViewHolder(View view) {
            super(view);

            sub_forum_name = (TextView) view.findViewById(R.id.sub_forum_name);
            sub_forum_description = (TextView) view.findViewById(R.id.sub_forum_description);
            divider = view.findViewById(R.id.dividerSubForum);

            subForumSubForumRecyclerView = (RecyclerView) view.findViewById(R.id.sub_forum_sub_forum);
            //subForumSubForumRecyclerView.setHasFixedSize(true);

            subForumSubForumLayoutManager = new LinearLayoutManager(context);
            subForumSubForumRecyclerView.setLayoutManager(subForumSubForumLayoutManager);

            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Intent intent = new Intent(context, ForumActivity.class);

            //intent.putExtra("auth_name", auth_name.getText().toString());
            //intent.putExtra("topic_title", post_title.getText().toString());
            intent.putExtra("forum_id", forum_id);
            intent.putExtra("sub_forum_name", sub_forum_name.getText());
            //Toast.makeText(context, sub_forum_sub_forum_name.getText(), Toast.LENGTH_LONG).show();
            context.startActivity(intent);
        }
    }

    public SubForumsAdapter(Context context, ArrayList<SubForum> listSubForum) {
        this.listSubForum = listSubForum;
        this.context = context;
    }

    @Override
    public SubForumsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View vi = LayoutInflater.from(parent.getContext()).inflate(R.layout.sub_forum_adapter, parent, false);
        // set the view's size, margins, paddings and layout parameters

        SubForumsAdapter.ViewHolder vh = new SubForumsAdapter.ViewHolder(vi);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(SubForumsAdapter.ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        SubForum subforum = listSubForum.get(position);
        //Picasso.with(holder.post_image.getContext()).load(image.getUrl()).into(holder.post_image);
        holder.sub_forum_name.setText(subforum.getName());

        if (subforum.getDescription().equals("")){
            holder.sub_forum_description.setVisibility(View.GONE);
        }else{
            holder.sub_forum_description.setText(subforum.getDescription());
        }

        if (subforum.getSub_forum().size() == 0){
            holder.divider.setVisibility(View.GONE);
        }

        holder.forum_id = subforum.getId();

        holder.subForumSubForumAdapter = new SubForumsSubForumsAdapter(context, subforum.getSub_forum());
        holder.subForumSubForumRecyclerView.setAdapter(holder.subForumSubForumAdapter);
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return (null != listSubForum? listSubForum.size() : 0);
    }

}
