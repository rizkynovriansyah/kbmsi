package id.ac.ub.filkom.kbmsi.wapss.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import id.ac.ub.filkom.kbmsi.wapss.Models.Post;
import id.ac.ub.filkom.kbmsi.wapss.R;
import id.ac.ub.filkom.kbmsi.wapss.Utilities.CircleTransform;
import id.ac.ub.filkom.kbmsi.wapss.Utilities.Util;

/**
 * Created by E5471 on 02/11/2016.
 */

public class TopicAdapter extends RecyclerView.Adapter<TopicAdapter.ViewHolder> {

    private ArrayList<Post> Topic = new ArrayList<>();
    private static Context context;

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView post_title_on_topic;
        public ImageView auth_pict_on_topic;
        public TextView auth_name_on_topic;
        public TextView auth_title_on_topic;
        public TextView starter_name_oc_topic;
        public TextView post_time_on_topic;
        public WebView post_content_on_topic;
        public TextView post_views_on_topic;
        public TextView post_replies_on_topic;


        public ViewHolder(View view) {
            super(view);
            post_title_on_topic = (TextView) view.findViewById(R.id.post_title_on_topic);
            auth_pict_on_topic = (ImageView) view.findViewById(R.id.auth_pict_on_topic);
            auth_name_on_topic = (TextView) view.findViewById(R.id.auth_name_on_topic);
            auth_title_on_topic = (TextView) view.findViewById(R.id.auth_title_on_topic);
            starter_name_oc_topic = (TextView) view.findViewById(R.id.starter_name_oc_topic);
            post_time_on_topic = (TextView) view.findViewById(R.id.post_time_on_topic);

            post_content_on_topic = (WebView) view.findViewById(R.id.post_content_on_topic);

            post_views_on_topic = (TextView) view.findViewById(R.id.post_views_on_topic);
            post_replies_on_topic = (TextView) view.findViewById(R.id.post_replies_on_topic);
        }
    }

    public TopicAdapter(Context context, ArrayList<Post> Topic) {
        this.Topic = Topic;
        this.context = context;
    }

    @Override
    public TopicAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View vi = LayoutInflater.from(parent.getContext()).inflate(R.layout.topic_adapter, parent, false);
        // set the view's size, margins, paddings and layout parameters

        TopicAdapter.ViewHolder vh = new TopicAdapter.ViewHolder(vi);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(TopicAdapter.ViewHolder holder, int position) {

        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        Post post = Topic.get(position);
        //Picasso.with(holder.post_image.getContext()).load(image.getUrl()).into(holder.post_image);

        if(!post.getPost_title().equals("")){
            holder.post_title_on_topic.setText(post.getPost_title());
        }else{
            holder.post_title_on_topic.setVisibility(View.GONE);
        }

        if(!post.getStarter_name().equals("")){
            holder.starter_name_oc_topic.setText(post.getStarter_name());
        }else{
            holder.post_title_on_topic.setVisibility(View.GONE);
        }

        if(!post.getPost_time().equals("")){
            holder.post_time_on_topic.setText(post.getPost_time());
        }else{
            holder.post_title_on_topic.setVisibility(View.GONE);
        }

        if(!post.getViews().equals("")){
            holder.post_views_on_topic.setText(post.getViews());
        }else{
            holder.post_title_on_topic.setVisibility(View.GONE);
        }

        if(!post.getReplies().equals("")){
            holder.post_replies_on_topic.setText(post.getReplies());
        }else{
            holder.post_title_on_topic.setVisibility(View.GONE);
        }



        Picasso.with(context)
                .load(post.getAuth_pict())
                .transform(new CircleTransform())
                .into(holder.auth_pict_on_topic);

        holder.auth_name_on_topic.setText(post.getAuth_name());
        holder.auth_title_on_topic.setText(post.getAuth_title());


        WebSettings settings = holder.post_content_on_topic.getSettings();
        settings.setMinimumFontSize(18);
        settings.setLoadWithOverviewMode(true);
        settings.setUseWideViewPort(true);
        settings.setBuiltInZoomControls(true);
        settings.setDisplayZoomControls(false);

        holder.post_content_on_topic.setWebChromeClient(new WebChromeClient());
        String changeFontHtml = Util.changedHeaderHtml(/*Util.bbcode(*/post.getPost_content()/*)*/);
        holder.post_content_on_topic.loadDataWithBaseURL(null, changeFontHtml,
                "text/html", "UTF-8", null);

        holder.post_content_on_topic.setVisibility(View.VISIBLE);

        //holder.post_content_on_topic.loadData(Util.bbcode(post.getPost_content()), "text/html", null);

        holder.post_content_on_topic.getSettings().setBuiltInZoomControls(true);


    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return (null != Topic ? Topic.size() : 0);
    }


}
