package id.ac.ub.filkom.kbmsi.wapss.Fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import id.ac.ub.filkom.kbmsi.wapss.Adapters.RecentTopicsAdapter;
import id.ac.ub.filkom.kbmsi.wapss.MainActivity;
import id.ac.ub.filkom.kbmsi.wapss.Models.PostImages;
import id.ac.ub.filkom.kbmsi.wapss.Models.RecentTopics;
import id.ac.ub.filkom.kbmsi.wapss.R;
import id.ac.ub.filkom.kbmsi.wapss.Utilities.Config;

/**
 * Created by E5471 on 06/11/2016.
 */

public class RecentTopicsFragment extends Fragment {

    private ArrayList<RecentTopics> RecentTopics = new ArrayList<>();
    private ProgressDialog dialog;

    private RecyclerView recentTopicsRecyclerView;
    private RecentTopicsAdapter recentTopicsAdapter;
    private RecyclerView.LayoutManager recentTopicsLayoutManager;

    private SwipeRefreshLayout recentTopicsSwipeRefreshLayout;

    public RecentTopicsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*((MainActivity) getActivity()).setActionBarTitle("Recent Topics");*/
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.recent_topics_fragment, container, false);

        // Replace 'android.R.id.list' with the 'id' of your RecyclerView
        recentTopicsRecyclerView = (RecyclerView) view.findViewById(R.id.recycleviewRecentTopics);
        recentTopicsLayoutManager = new LinearLayoutManager(getActivity());

        recentTopicsSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout);

        recentTopicsSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                recentTopicsRecyclerView.stopScroll();
                // Refresh items
                refreshItems();
            }
        });

        recentTopicsRecyclerView.setLayoutManager(recentTopicsLayoutManager);

        getRecentTopics();

        return view;
    }

    public void getRecentTopics(){
        dialog = ProgressDialog.show(getActivity(),"","Loading...");
        RequestQueue queue = Volley.newRequestQueue(this.getActivity());

        // Instantiate the RequestQueue.
        JsonArrayRequest jsArrRequest = new JsonArrayRequest
                (Request.Method.GET, Config.API_RECENT_TOPICS, null, new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {
                        //Toast.makeText(getActivity(), response.toString(), Toast.LENGTH_LONG).show();
                        jsonParseRecentTopics(response);
                    }

                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getActivity(), "Recent Topics tidak tersedia, periksa Jaringan Anda", Toast.LENGTH_LONG).show();
                    }
                });

        queue.add(jsArrRequest);
    }

    public void clearData() {
        /*int size = this.RecentTopics.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                this.RecentTopics.remove(0);
            }
        }*/
        this.RecentTopics.clear();
    }

    void refreshItems() {
        clearData();

        getRecentTopics();
        // Load complete
        onItemsLoadComplete();
    }

    void onItemsLoadComplete() {
        // Stop refresh animation
        recentTopicsSwipeRefreshLayout.setRefreshing(false);
    }

    public void jsonParseRecentTopics(JSONArray json){

        if (!json.equals("null")){
            try {
                for (int i = 0; i < json.length(); i++ ){

                    ArrayList<PostImages> listPostImages = new ArrayList<>();
                    JSONObject object = json.getJSONObject(i);
                    if (object != null){
                        JSONArray postImages = object.getJSONArray("images");

                        if (postImages != null){
                            for (int j = 0; j  < postImages.length(); j++){
                                JSONObject obj = postImages.getJSONObject(j);

                                listPostImages.add(new PostImages(obj.getString("url")));

                            }
                        }
                    }

                    RecentTopics.add(
                            new RecentTopics(
                                    object.getInt("post_id"),
                                    object.getString("topic_id"),
                                    object.getString("auth_pict"),
                                    object.getString("auth_name"),
                                    object.getString("auth_title"),
                                    object.getString("sub_forum"),
                                    object.getString("post_title"),
                                    listPostImages,
                                    object.getString("views"),
                                    object.getString("replies"),
                                    object.getString("post_time")
                            )
                    );

                }

                recentTopicsAdapter = new RecentTopicsAdapter(this.getActivity(), RecentTopics);
                recentTopicsRecyclerView.setAdapter(recentTopicsAdapter);

            }catch (JSONException e){
                e.printStackTrace();
            }
        }
        dialog.dismiss();
    }
}
