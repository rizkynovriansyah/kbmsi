package id.ac.ub.filkom.kbmsi.wapss.Fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import id.ac.ub.filkom.kbmsi.wapss.Adapters.SubForumsAdapter;
import id.ac.ub.filkom.kbmsi.wapss.MainActivity;
import id.ac.ub.filkom.kbmsi.wapss.Models.SubForum;
import id.ac.ub.filkom.kbmsi.wapss.R;
import id.ac.ub.filkom.kbmsi.wapss.Utilities.Config;

/**
 * Created by E5471 on 06/11/2016.
 */

public class SubForumFragment extends Fragment {

    ProgressDialog dialog;

    ArrayList<SubForum> subForums = new ArrayList<>();
    private ArrayList<id.ac.ub.filkom.kbmsi.wapss.Models.RecentTopics> RecentTopics = new ArrayList<>();

    private RecyclerView subForumRecyclerView;
    private SubForumsAdapter subForumAdapter;
    private RecyclerView.LayoutManager subForumLayoutManager;

    private SwipeRefreshLayout forumSwipeRefreshLayout;

    public SubForumFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((MainActivity) getActivity()).setActionBarTitle("Recent Topics");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.sub_forum_fragment, container, false);

        // Replace 'android.R.id.list' with the 'id' of your RecyclerView
        subForumRecyclerView = (RecyclerView) view.findViewById(R.id.recycleviewSubForum);
        subForumLayoutManager = new LinearLayoutManager(getActivity());

        //Log.d("debugMode", "The application stopped after this");
        subForumRecyclerView.setLayoutManager(subForumLayoutManager);

        forumSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.subForumSwipeRefreshLayout);

        forumSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                subForumRecyclerView.stopScroll();
                // Refresh items
                refreshItems();
            }
        });

        getForum();

        // Inflate the layout for this fragment
        return view;
    }

    void refreshItems() {

        clearData();
        getForum();
        // Load complete
        onItemsLoadComplete();
    }

    public void clearData() {
        /*int size = this.subForums.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                this.subForums.remove(0);
            }
        }*/

        this.subForums.clear();
    }


    void onItemsLoadComplete() {
        // Stop refresh animation
        forumSwipeRefreshLayout.setRefreshing(false);
    }

    public void getForum() {
        dialog = ProgressDialog.show(getActivity(), "", "Loading...");
        RequestQueue queue = Volley.newRequestQueue(this.getActivity());

        // Instantiate the RequestQueue.
        JsonArrayRequest jsArrRequest = new JsonArrayRequest
                (Request.Method.GET, Config.API_ALL_SUB_FORUM, null, new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {
                        //Toast.makeText(ForumActivity.this, response.toString(), Toast.LENGTH_LONG).show();
                        jsonParseSubForum(response);
                    }

                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getActivity(), "Forum tidak tersedia", Toast.LENGTH_LONG).show();
                    }
                });

        queue.add(jsArrRequest);
    }

    public void jsonParseSubForum(JSONArray response) {
        if (response != null) {
            /*try{
                System.out.println(response.getJSONObject(response.length()-1).toString());
            }catch (JSONException e){
                e.printStackTrace();
            }*/

            try {
                for (int i = 0; i < response.length(); i++){
                    //Log.d("hmm", "" + i);
                    JSONObject object = response.getJSONObject(i);
                    //Log.d("hmm", object.toString());
                    ArrayList<SubForum> subSubForum = new ArrayList<>();

                    JSONArray json_sub_forum = object.getJSONArray("sub_forum");
                        /*Log.d("sub_forum", object.getJSONArray("sub_forum").toString());*/
                    //JSONArray json_sub_forum = object.getJSONArray("sub_forum");

                    for (int a = 0; a < json_sub_forum.length(); a++){
                        JSONObject obj = json_sub_forum.getJSONObject(a);

                        if (obj.getInt("id") != -2){ //wanjjjeeerrrr ini perlu dibenahi!!!
                            subSubForum.add(
                                    new SubForum(
                                            obj.getString("id"),
                                            obj.getString("name"),
                                            obj.getString("description"),
                                            null
                                    ));
                        }
                        //Log.d("sub_forum", "" + a);
                    }
                    subForums.add(new SubForum(
                            object.getString("id"),
                            object.getString("name"),
                            object.getString("description"),
                            subSubForum
                    ));

                    subForumAdapter = new SubForumsAdapter(this.getActivity(), subForums);
                    subForumRecyclerView.setAdapter(subForumAdapter);
                }
            }catch (JSONException e) {
                e.printStackTrace();
            }
        }
        dialog.dismiss();
    }

}
