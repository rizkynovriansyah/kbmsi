package id.ac.ub.filkom.kbmsi.wapss.Models;

public class Post {
    private String post_id;
    private String sub_forum;
    private String post_title;
    private String auth_pict;
    private String auth_name;
    private String auth_title;
    private String starter_name;
    private String post_time;
    private String post_content;
    private String views;
    private String replies;

    public Post(String post_id, String sub_forum, String post_title, String auth_pict, String auth_name, String auth_title, String starter_name, String post_time, String post_content, String views, String replies) {
        this.post_id = post_id;
        this.sub_forum = sub_forum;
        this.post_title = post_title;
        this.auth_pict = auth_pict;
        this.auth_name = auth_name;
        this.auth_title = auth_title;
        this.starter_name = starter_name;
        this.post_time = post_time;
        this.post_content = post_content;
        this.views = views;
        this.replies = replies;
    }

    public String getPost_id() {
        return post_id;
    }

    public String getSub_forum() {
        return sub_forum;
    }

    public String getPost_title() {
        return post_title;
    }

    public String getAuth_pict() {
        return auth_pict;
    }

    public String getAuth_name() {
        return auth_name;
    }

    public String getAuth_title() {
        return auth_title;
    }

    public String getStarter_name() {
        return starter_name;
    }

    public String getPost_time() {
        return post_time;
    }

    public String getPost_content() {
        return post_content;
    }

    public String getViews() {
        return views;
    }

    public String getReplies() {
        return replies;
    }
}
