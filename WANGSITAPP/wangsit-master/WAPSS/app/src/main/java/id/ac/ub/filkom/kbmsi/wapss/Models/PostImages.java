package id.ac.ub.filkom.kbmsi.wapss.Models;

/**
 * Created by E5471 on 29/10/2016.
 */

public class PostImages {
    private String url;

    public PostImages(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }
}
