package id.ac.ub.filkom.kbmsi.wapss.Models;

import java.util.ArrayList;


/**
 * Created by E5471 on 29/10/2016.
 */

public class RecentTopics {

    private int post_id;
    private String topic_id;
    private String auth_pict;
    private String auth_name;
    private String auth_title;
    private String sub_forum;
    private String post_title;
    private ArrayList<PostImages> post_images;
    private String views;
    private String replies;
    private String post_time;

    public RecentTopics () {

    }

    public RecentTopics(int post_id, String topic_id, String auth_pict, String auth_name, String auth_title, String sub_forum, String post_title, ArrayList<PostImages> post_images, String views, String replies, String post_time) {
        this.post_id = post_id;
        this.topic_id = topic_id;
        this.auth_pict = auth_pict;
        this.auth_name = auth_name;
        this.auth_title = auth_title;
        this.sub_forum = sub_forum;
        this.post_title = post_title;
        this.post_images = post_images;
        this.views = views;
        this.replies = replies;
        this.post_time = post_time;
    }

    public int getPost_id() {
        return post_id;
    }

    public String getTopic_id() {
        return topic_id;
    }

    public String getAuth_pict() {
        return auth_pict;
    }

    public String getAuth_name() {
        return auth_name;
    }

    public String getAuth_title() {
        return auth_title;
    }

    public String getSub_forum() {
        return sub_forum;
    }

    public String getPost_title() {
        return post_title;
    }

    public ArrayList<PostImages> getPost_images() {
        return post_images;
    }

    public String getViews() {
        return views;
    }

    public String getReplies() {
        return replies;
    }

    public String getPost_time() {
        return post_time;
    }
}
