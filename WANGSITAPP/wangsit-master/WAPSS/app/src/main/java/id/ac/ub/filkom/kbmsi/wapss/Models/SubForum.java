package id.ac.ub.filkom.kbmsi.wapss.Models;

import java.util.ArrayList;

/**
 * Created by E5471 on 01/11/2016.
 */

public class SubForum {

    private String id;
    private String name;
    private String description;

    private ArrayList<SubForum> sub_forum;

    public SubForum(String id, String name, String description, ArrayList<SubForum> sub_forum) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.sub_forum = sub_forum;
    }

    public SubForum(String id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public ArrayList<SubForum> getSub_forum() {
        return sub_forum;
    }
}
