package id.ac.ub.filkom.kbmsi.wapss.Utilities;

/**
 * Created by E5471 on 31/10/2016.
 */

public class Config {

    public static final String URL_DOMAIN = "http://kbmsi.filkom.ub.ac.id/json/";
    public static final String URL_DOMAIN_LOCAL = "http://192.168.1.103/jsonwangsit/";

    public static final String API_RECENT_TOPICS = URL_DOMAIN + "recent_topics2.php";
    public static final String API_ALL_SUB_FORUM = URL_DOMAIN + "allforum.php";
    public static final String API_GET_TOPIC = URL_DOMAIN + "get_topic.php?topic_id=";
    public static final String API_UPDATE_VIEWERS = URL_DOMAIN + "update_viewers.php?topic_id=";
    public static final String API_GET_SUB_FORUM = URL_DOMAIN + "subforum.php?id=";

    public static final String API_GET_TOPIC_ON_SUB_FORUM = URL_DOMAIN + "get_topic_on_sub_forum.php?forum_id=";

    public static final String API_RECENT_TOPICS_LOCAL = URL_DOMAIN_LOCAL + "recent_topics.php";

}
